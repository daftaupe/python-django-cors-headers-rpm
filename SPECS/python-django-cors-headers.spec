# Created by pyp2rpm-3.3.2
%global pypi_name django-cors-headers

Name:           python-%{pypi_name}
Version:        3.2.1
Release:        1%{?dist}
Summary:        django-cors-headers is a Django application for handling the server headers required for Cross-Origin Resource Sharing (CORS)

License:        MIT License
URL:            https://github.com/adamchainz/django-cors-headers
Source0:        https://files.pythonhosted.org/packages/source/d/%{pypi_name}/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch
 
BuildRequires:  python3-devel
BuildRequires:  python3dist(django) >= 1.11
BuildRequires:  python3dist(setuptools)

%description
django-cors-headers A Django App that adds Cross-Origin Resource Sharing (CORS)
headers to responses. This allows in-browser requests to your Django
application from other origins.About CORS -Adding CORS headers allows your
resources to be accessed on other domains. It's

%package -n     python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}
 
Requires:       python3dist(django) >= 1.11
%description -n python3-%{pypi_name}
django-cors-headers A Django App that adds Cross-Origin Resource Sharing (CORS)
headers to responses. This allows in-browser requests to your Django
application from other origins.About CORS -Adding CORS headers allows your
resources to be accessed on other domains. It's


%prep
%autosetup -n %{pypi_name}-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%py3_build

%install
%py3_install

%files -n python3-%{pypi_name}
%license LICENSE
%doc README.rst
%{python3_sitelib}/corsheaders
%{python3_sitelib}/django_cors_headers-%{version}-py?.?.egg-info

%changelog
* Sun Jan 19 2020 mockbuilder - 3.2.1-1
- Initial package.
